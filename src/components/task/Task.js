import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { Container, Grid, Input, Button, TableHead, Table, TableRow, TableCell, TableBody } from '@mui/material';

const Task = () => {
  const dispatch = useDispatch();

  const { tasks, taskName, filterTaskName } = useSelector((reduxData) => reduxData.taskReducer);

  const taskNameHandler = event => {
    dispatch({
      type: "VALUE_HANDLER",
      payload: {
        taskName: event.target.value
      }
    });
  };

  const addTaskHandler = event => {
    event.preventDefault();
    let id = 0;

    // Hàm xử lý để lấy id cho task mới
    if (tasks.length) { 
      id = Math.max(...tasks.map(task => task.id));
      id++;
    }

    if (taskName) {
      dispatch({
        type: "ADD_TASK",
        payload: {
          task: {
            name: taskName,
            completed: false,
            id
          }
        }
      });
      dispatch({
        type: "VALUE_HANDLER",
        payload: {
          taskName: ""
        }
      });
    }
  };

  const taskCompletedHandler = (id) => { 
    dispatch({
      type: "TOGGLE_COMPLETED_TASK",
      payload: {
        id: id
      }
    });
  };
  const filterChangeHandel = event => {
    dispatch({
      type: "CHANGE_FILTER_NAME",
      payload: {
        filterTaskName: event.target.value,
      }
    });
    
  }

  return (
    <Container>
      <Grid container spacing={2} style={{ margin: '20px auto', padding: "15px", border:"solid",width:"750px" }}>
        <Grid item xs={12}>
          <form onSubmit={addTaskHandler} style={{width:"800px"}}>
            <Grid container item xs={12}>
              <Grid item xs={8}>
                <Input value={taskName} onChange={taskNameHandler} style={{ width: '90%' }} />
              </Grid>
              <Grid item xs={4}>
              <Button variant="contained" type="submit" align="center">Add Task</Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>

      <Grid container spacing={2} style={{ margin: '20px auto', padding: "15px", border:"solid",width:"750px" }}>
        <Grid item xs={12}>
          <form style={{width:"800px"}}>
            <Grid container item xs={12}>  
              <Grid item xs={8}>
                <Input value={filterTaskName} onChange={filterChangeHandel} style={{ width: '90%' }} />
              </Grid>
              <Grid item xs={2} align="center">Filter</Grid>
            </Grid>
          </form>
        </Grid>
        </Grid>


      <Table sx={{ minWidth: 350, border: 1 }}>
        <TableHead sx={{ backgroundColor: "#61481C" }}>
          <TableRow >
            <TableCell sx={{ border: 1, width: "200px",color:"white" }}>STT</TableCell>
            <TableCell sx={{ border: 1, color:"white" }} align="left">Nội dung</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {
        (tasks.filter(task=>task.name.includes(filterTaskName)))
        .map((task, index) => {
          return (
            <TableRow
              key={task.id}
                /*   style={{
                    color: task.completed ? "green " : "red "
                  }} */
                  onClick={()=>taskCompletedHandler(task.id)}
                  id={task.id}
            >
              <TableCell sx={{ border: 1,color: task.completed ? "green " : "red " }} >
                {index + 1} 
              </TableCell>
              <TableCell sx={{ border: 1,color: task.completed ? "green " : "red " }}>
                {task.name}
              </TableCell>
            </TableRow>
          );
        })
        }
         </TableBody>
      </Table>
    </Container>
  );
}

export default Task;

