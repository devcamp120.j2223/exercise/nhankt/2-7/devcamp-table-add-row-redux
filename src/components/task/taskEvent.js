

const initialState = {
  taskName: "",
  tasks: [],
  filterTaskName: "",
  filterDisplay : false,
  filterTask: []
};

const taskEvent = (state = initialState, action) => {
  switch (action.type) {
    case "VALUE_HANDLER": {
      return {
        ...state,
        taskName: action.payload.taskName
      };
    }
    case "ADD_TASK": {

      return {
        ...state,
        tasks: [...state.tasks, action.payload.task],
        filterDisplay : false
      };
    }
    case "TOGGLE_COMPLETED_TASK": {
      const toggleTask = task => {
        if (task.id.toString() === action.payload.id.toString()) {
          task.completed = !task.completed;
        }
        return task;
      };
      state.tasks.forEach(element => {
        if (element.id.toString()=== action.payload.id.toString()){
          return !element.completed;
        }
      });

      return {
        ...state,
        tasks: state.tasks.map(toggleTask)
      };
    }
    case "CHANGE_FILTER_NAME" : {
      return {
        ...state,
        filterTaskName: action.payload.filterTaskName
      };
    }
    default: {
      return state;
    }
  }
};

export default taskEvent;
